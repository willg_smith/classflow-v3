package com.wgsdev.classflowv3.ui.gradebook;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.Assignment;
import com.wgsdev.classflowv3.database.Class;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.database.Semester;
import com.wgsdev.classflowv3.ui.assignments.AssignmentAdapter;
import com.wgsdev.classflowv3.ui.dialogs.EditAssignmentDialog;

import java.util.List;

public class ShareFragment extends Fragment {

    private List<Class> classList;
    private List<Assignment> assignmentList;
    private View view;

    AppDatabase db = null;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.gradebook_home, container, false);

        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        classList = db.classDAO().getAllClasses();

        String[] classNames = new String[classList.size()];
        for (int i = 0; i < classList.size(); i++) {
                classNames[i] = classList.get(i).getName();
            }

        Spinner spinner = view.findViewById(R.id.gradebook_class_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner, classNames);
        adapter.setDropDownViewResource(R.layout.custom_spinner);
        spinner.setAdapter(adapter);
        spinner.setEnabled(true);
        spinner.setSelection(0);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String name = adapterView.getItemAtPosition(i).toString();
                    Class selectedClass = null;

                    for (int j = 0; j < classList.size(); j++) {
                        if (classList.get(j).getName().equals(name)) {
                            selectedClass = classList.get(j);
                        }
                    }

                    assignmentList = db.assignmentDAO().getAssignmentsByClass(selectedClass.getClassID());


                    RecyclerView listOfAssignments = getActivity().findViewById(R.id.gradebook_recycler);
                    listOfAssignments.setLayoutManager(new LinearLayoutManager(getContext()));
                    AssignmentAdapter listAdapter = new AssignmentAdapter(assignmentList, new AssignmentAdapter.SelectedAssignment() {
                        @Override
                        public void selectedAssignment(Assignment assignment) {
                            EditAssignmentDialog add = new EditAssignmentDialog(assignment);
                            add.show(getActivity().getSupportFragmentManager(), "Edit Assignment");
                        }
                    });
                    listOfAssignments.setAdapter(listAdapter);
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                    return;
                }
            });

        return view;

}
}