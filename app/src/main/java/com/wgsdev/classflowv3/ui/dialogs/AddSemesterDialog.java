package com.wgsdev.classflowv3.ui.dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.wgsdev.classflowv3.MainActivity;
import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.database.Semester;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddSemesterDialog extends AppCompatDialogFragment {

    EditText name;
    EditText startDate;
    EditText endDate;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflator = getActivity().getLayoutInflater();
        View view = inflator.inflate(R.layout.layout_add_semester_dialog, null);
        AppDatabase db = null;
        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        name = (EditText) view.findViewById(R.id.semester_name_d);
        startDate = (EditText) view.findViewById(R.id.start_date_d);
        endDate = (EditText) view.findViewById(R.id.end_date_d);


        final AppDatabase finalDb = db;
        builder.setView(view).setTitle("Add Semester").setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date start = null;
                Date end = null;
                try {
                    start = new Date(String.valueOf(formatter.parse(String.valueOf(startDate.getText()))));
                    end = new Date(String.valueOf(formatter.parse(String.valueOf(endDate.getText()))));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long startMilli = start.getTime();
                long endMilli = end.getTime();
                String sName = String.valueOf(name.getText());
                Semester temp = new Semester(sName, startMilli, endMilli);
                finalDb.semesterDAO().insert(temp);


            }
        });


        return builder.create();
    }

    private DialogInterface.OnDismissListener onDismissListener;

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }
}
