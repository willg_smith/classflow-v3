package com.wgsdev.classflowv3.ui.upcoming;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.Assignment;
import com.wgsdev.classflowv3.database.Class;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.ui.dialogs.EditAssignmentDialog;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SlideshowFragment extends Fragment {

    LinearLayout futureAssignments;
    LinearLayout pastAssignments;
    Context context;
    AppDatabase db;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.upcoming_assignments_home, container, false);

        context = getContext();
        futureAssignments = view.findViewById(R.id.future_assignments_home);
        pastAssignments = view.findViewById(R.id.past_assignments_home);

        List<View> futureCards = createFutureCards(context);
        List<View> pastCards = createPastCards(context);


        for (int i = 0; i < futureCards.size(); i++) {
            futureAssignments.addView(futureCards.get(i));
        }

        for (int i = 0; i < pastCards.size(); i++) {
            pastAssignments.addView(pastCards.get(i));
        }

        return view;
    }

    private List<View> createPastCards(Context context) {
        List<View> result = new ArrayList<View>();

        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Date current = new Date();
        long time = current.getTime();
        List<Assignment> assignments;

        assignments = db.assignmentDAO().getAllPastAssignments(time);

        for (int i = 0; i < assignments.size(); i++) {
            final Assignment assTemp = assignments.get(i);
            Class classTemp = db.classDAO().getClass(assTemp.getClassID());

            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = vi.inflate(R.layout.assignment_list_layout_home, null);

            TextView assignmentName = view.findViewById(R.id.assignment_name_home);
            TextView className = view.findViewById(R.id.assignment_class_name_home);
            TextView dueDate = view.findViewById(R.id.assignment_due_date_home);
            TextView note = view.findViewById(R.id.assignment_note_home);
            TextView grade = view.findViewById(R.id.assignment_grade_home);

            assignmentName.setText(assTemp.getName());
            className.setText(classTemp.getName());
            if (!StringUtils.isAllBlank(assTemp.getNote())) {
                note.setText(assTemp.getNote());
            }
            String points = " - " + assTemp.getPointsEarned() + "/" + assTemp.getPointsPossible() + " points";
            grade.setText(points);

            Date assignmentDate = new Date(assTemp.getDueDate());
            int daysUntilDue = (int) ((assignmentDate.getTime() - time)/(1000*60*24*60)) + 1;
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            String resultDays = "";

            if(true) {
                if (daysUntilDue < 0 && !assTemp.isCompleted()) {
                    resultDays = "Due on " + format.format(assignmentDate) + " - past due by " + (Math.abs(daysUntilDue)) + " days";
                }
                else if (assTemp.isCompleted()) {
                    resultDays = "Assignment complete - due on " + format.format(assignmentDate);
                }
                else if (daysUntilDue >= 0){
                    resultDays = "Due on " + format.format(assignmentDate) + " - " + daysUntilDue + " days";
                }
            }
            dueDate.setText(resultDays);

            View.OnClickListener click = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditAssignmentDialog add = new EditAssignmentDialog(assTemp);

                    add.show(getActivity().getSupportFragmentManager(), "Edit Assignment");
                }
            };

            view.setOnClickListener(click);

            result.add(view);
        }
        return result;
    }

    private List<View> createFutureCards(Context context) {
        List<View> result = new ArrayList<View>();

        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Date current = new Date();
        long time = current.getTime();
        List<Assignment> assignments;

        assignments = db.assignmentDAO().getAllUpcomingAssignments(time);

        for (int i = 0; i < assignments.size(); i++) {
            final Assignment assTemp = assignments.get(i);
            Class classTemp = db.classDAO().getClass(assTemp.getClassID());

            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = vi.inflate(R.layout.assignment_list_layout_home, null);

            TextView assignmentName = view.findViewById(R.id.assignment_name_home);
            TextView className = view.findViewById(R.id.assignment_class_name_home);
            TextView dueDate = view.findViewById(R.id.assignment_due_date_home);
            TextView note = view.findViewById(R.id.assignment_note_home);
            TextView grade = view.findViewById(R.id.assignment_grade_home);

            assignmentName.setText(assTemp.getName());
            className.setText(classTemp.getName());
            if (!StringUtils.isAllBlank(assTemp.getNote())) {
                note.setText(assTemp.getNote());
            }
            note.setText(assTemp.getNote());
            String points = " - " + assTemp.getPointsEarned() + "/" + assTemp.getPointsPossible() + " points";
            grade.setText(points);

            Date assignmentDate = new Date(assTemp.getDueDate());
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

            int daysUntilDue = (int) ((assignmentDate.getTime() - time)/(1000*60*24*60)) + 1;
            String resultDays = "Due on " + format.format(assignmentDate) + " - " + daysUntilDue + " days";;
            dueDate.setText(resultDays);

            View.OnClickListener click = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditAssignmentDialog add = new EditAssignmentDialog(assTemp);
                    add.show(getActivity().getSupportFragmentManager(), "Edit Assignment");
                }
            };

            view.setOnClickListener(click);

            result.add(view);
        }
        return result;
    }
}