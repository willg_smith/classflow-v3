package com.wgsdev.classflowv3.ui.semesters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.Semester;

import java.util.List;

public class SemesterAdapter extends RecyclerView.Adapter<SemesterAdapter.SemesterViewHolder> {

    private List<Semester> semesterList;
    private Context context;
    private SelectedSemester selectedSemester;

    public SemesterAdapter(List<Semester> semesterList, SelectedSemester selectedSemester) {
        this.semesterList = semesterList;
        this.selectedSemester = selectedSemester;
    }

    @NonNull
    @Override
    public SemesterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.semester_list_layout, null);
        SemesterViewHolder holder = new SemesterViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SemesterViewHolder holder, int position) {

        Semester semester = semesterList.get(position);
        holder.semesterTitle.setText(semester.getName());
        String gpa = "GPA: in progress";
        holder.semesterGPA.setText(gpa);

    }

    @Override
    public int getItemCount() {
        return semesterList.size();
    }

    class SemesterViewHolder extends RecyclerView.ViewHolder {
        TextView semesterTitle;
        TextView semesterGPA;

        public SemesterViewHolder(@NonNull View itemView) {
            super(itemView);

            semesterTitle = itemView.findViewById(R.id.semester_name);
            semesterGPA = itemView.findViewById(R.id.semester_gpa);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedSemester.selectedSemester(semesterList.get(getAdapterPosition()));
                }
            });
        }
    }

    public void setData(List<Semester> newData) {
        semesterList = newData;
        notifyDataSetChanged();
    }

    public interface SelectedSemester {
        void selectedSemester(Semester semester);
    }


}
