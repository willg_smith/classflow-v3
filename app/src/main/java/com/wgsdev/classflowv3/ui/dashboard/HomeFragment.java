package com.wgsdev.classflowv3.ui.dashboard;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.Assignment;
import com.wgsdev.classflowv3.database.Class;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HomeFragment extends Fragment {

    LinearLayout upcomingAssignments;
    Context context;
    AppDatabase db = null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_home, container, false);

        context = getContext();
        upcomingAssignments = view.findViewById(R.id.upcoming_assignments_dashboard);

        List<View> cards = createCards(context);
        for (int i = 0; i < cards.size(); i++) {
            upcomingAssignments.addView(cards.get(i));
        }



        return view;
    }

    private List<View> createCards(Context context) {
        List<View> result = new ArrayList<View>();
        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Date current = new Date();
        long time = current.getTime();
        List<Assignment> assignments;
        assignments = db.assignmentDAO().getAllUpcomingAssignments(time);

        for(int i = 0; i < assignments.size(); i++) {
            Assignment assTemp = assignments.get(i);
            Class classTemp = db.classDAO().getClass(assTemp.getClassID());

            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = vi.inflate(R.layout.layout_upcoming_assignments_dashboard, null);

            TextView assignmentName = view.findViewById(R.id.assignment_name_dashboard);
            TextView className = view.findViewById(R.id.class_name_dashboard);
            TextView days = view.findViewById(R.id.assignment_days_dashboard);
            assignmentName.setText(assTemp.getName());
            className.setText(classTemp.getName());
            Date date = new Date(assTemp.getDueDate());

            int daysUntilDue = (int) ((date.getTime() - time)/(1000*60*24*60)) + 1;
            String resultDays = daysUntilDue + " days";
            days.setText(resultDays);
            result.add(view);

        }
        return result;

    }
}