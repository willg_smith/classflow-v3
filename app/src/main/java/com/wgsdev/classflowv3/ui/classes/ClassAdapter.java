package com.wgsdev.classflowv3.ui.classes;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.Assignment;
import com.wgsdev.classflowv3.database.Class;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.database.Semester;
import com.wgsdev.classflowv3.ui.semesters.SemesterAdapter;


import java.util.ArrayList;
import java.util.List;

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.ClassViewHolder> {

    List<Class> classList;
    private Context context;
    private ClassAdapter.SelectedClass selectedClass;

    public ClassAdapter(List<Class> classList, SelectedClass selectedClass) {
        this.classList = classList;
        this.selectedClass = selectedClass;
    }

    @NonNull
    @Override
    public ClassAdapter.ClassViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.class_list_layout, parent, false);
        return new ClassViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassAdapter.ClassViewHolder holder, int position) {
        holder.className.setText(classList.get(position).getName());
        String grade = "Grade in progress";
        try {
            grade = getClassGrade(position);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        holder.classGrade.setText(grade);

    }

    private String getClassGrade(int position) throws Exception {
        Class temp = classList.get(position);
        AppDatabase db = DatabaseHolder.getRoomDatabase();
        List<Assignment> assignments = db.assignmentDAO().getAssignmentsByClass(temp.getClassID());
        int totalPointsPossible = 0;
        int pointsEarned = 0;
        for (Assignment ass : assignments) {
            totalPointsPossible += ass.getPointsPossible();
            pointsEarned += ass.getPointsEarned();
        }

        if (totalPointsPossible != 0) {
            double percent = Math.round(((double)pointsEarned / (double)totalPointsPossible) * 100.0);
            return pointsEarned + "/" + totalPointsPossible + " - " + percent + "%";
        } else {
            return "No assignments";
        }


    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public class ClassViewHolder extends RecyclerView.ViewHolder {
        public TextView className;
        public TextView classGrade;
        public ClassViewHolder(@NonNull View itemView) {
            super(itemView);
            className = itemView.findViewById(R.id.class_name);
            classGrade = itemView.findViewById(R.id.class_grade);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedClass.selectedClass(classList.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface SelectedClass {
        void selectedClass(Class aClass);
    }

    public void setData(List<Class> classList) {
        this.classList = classList;
    }
}


