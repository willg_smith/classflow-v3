package com.wgsdev.classflowv3.ui.assignments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.Assignment;
import com.wgsdev.classflowv3.database.Class;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.ui.classes.ClassAdapter;
import com.wgsdev.classflowv3.ui.dialogs.AddAssignmentDialog;
import com.wgsdev.classflowv3.ui.dialogs.AddClassDialog;
import com.wgsdev.classflowv3.ui.dialogs.EditAssignmentDialog;

import java.util.List;

public class AssignmentView extends AppCompatActivity implements AssignmentAdapter.SelectedAssignment {

    RecyclerView recyclerView;
    AssignmentAdapter adapter;
    List<Assignment> assignmentList;
    Button addAssignment;
    int classID;
    AppDatabase db = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_view);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        Class aClass = (Class) intent.getSerializableExtra("data");

        Toolbar tb = findViewById(R.id.assignments_toolbar);
        tb.setTitle(aClass.getName());
        classID = aClass.getClassID();

        tb.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addAssignment = findViewById(R.id.add_assignment);
        addAssignment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assignmentList = db.assignmentDAO().getAssignmentsByClass(aClass.getClassID());

        recyclerView = findViewById(R.id.assignmentsRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AssignmentAdapter(assignmentList, this);
        recyclerView.setAdapter(adapter);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.assignmentcolor));
    }

    private void openDialog() {
        AddAssignmentDialog add = new AddAssignmentDialog(classID);
        add.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                adapter.setData(db.assignmentDAO().getAssignmentsByClass(classID));
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }
        });
        add.show(getSupportFragmentManager(), "Add Assignment");
    }

    @Override
    public void selectedAssignment(Assignment assignment) {
        EditAssignmentDialog add = new EditAssignmentDialog(assignment);
        add.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                adapter.setData(db.assignmentDAO().getAssignmentsByClass(classID));
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }
        });
        add.show(getSupportFragmentManager(), "Edit Assignment");
    }
}
