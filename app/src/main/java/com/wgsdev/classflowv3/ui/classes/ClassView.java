package com.wgsdev.classflowv3.ui.classes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.widget.Toolbar;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.Class;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.database.Semester;
import com.wgsdev.classflowv3.ui.assignments.AssignmentView;
import com.wgsdev.classflowv3.ui.dialogs.AddClassDialog;
import com.wgsdev.classflowv3.ui.dialogs.AddSemesterDialog;

import java.util.List;

public class ClassView extends AppCompatActivity implements ClassAdapter.SelectedClass {

    RecyclerView recyclerView;
    ClassAdapter adapter;
    List<Class> classList;
    Button addClass;
    private int semesterID;
    AppDatabase db = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_view);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        Semester semester = (Semester) intent.getSerializableExtra("data");
        semesterID = semester.getId();


        Toolbar tb = findViewById(R.id.classes_toolbar);
        tb.setTitle(semester.getName());

        tb.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addClass = findViewById(R.id.add_class);

        addClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        classList = db.classDAO().getClassesBySemesterID(semesterID);

        recyclerView = findViewById(R.id.classRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ClassAdapter(classList, this);
        recyclerView.setAdapter(adapter);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.classcolor));


    }

    private void openDialog() {
        AddClassDialog add = new AddClassDialog(semesterID);
        add.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                adapter.setData(db.classDAO().getClassesBySemesterID(semesterID));
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }
        });
        add.show(getSupportFragmentManager(), "Add Class");
    }

    @Override
    public void selectedClass(Class aClass) {
        startActivity(new Intent(this, AssignmentView.class).putExtra("data", aClass));

    }
}
