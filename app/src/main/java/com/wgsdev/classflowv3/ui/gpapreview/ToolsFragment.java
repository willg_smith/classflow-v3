package com.wgsdev.classflowv3.ui.gpapreview;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.Class;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.database.Semester;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ToolsFragment extends Fragment {
    List<Class> classList;
    List<Semester> semesterList;
    List<View> viewList;
    Button calculate;
    TextView gpaResult;
    Context context;
    LinearLayout classes;

    public View onCreateView(@NonNull final LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.gpa_preview_home, container, false);
        AppDatabase db = null;
        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        context = getContext();
        gpaResult = view.findViewById(R.id.gpa_result);
        calculate = view.findViewById(R.id.calculateGPA);
        classes = view.findViewById(R.id.input_classes_gb);

        semesterList = db.semesterDAO().getAllSemesters();
        classList = db.classDAO().getAllClasses();

        viewList = createViews();

        for (int i = 0; i < viewList.size(); i++) {
            classes.addView(viewList.get(i));
        }

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int totalHours = 0;
                int totalGradePoints = 0;
                EditText hoursInput;
                EditText gradeInput;

                for (View view : viewList) {
                    hoursInput = view.findViewById(R.id.class_hours_gb);
                    gradeInput = view.findViewById(R.id.class_grade_gb);
                    totalHours += Integer.parseInt(hoursInput.getText().toString());
                    totalGradePoints += findGrade(gradeInput.getText().toString());
                }

                String gpaResultR = "GPA: " + ((double)(totalGradePoints*3)) / (double)totalHours;
                gpaResult.setText(gpaResultR);
            }
        });



        return view;
    }

    private int findGrade(String grade) {
        int result = 0;
        grade.toUpperCase();

        switch (grade) {
            case "A":
                result = 4;
                break;
            case "B":
                result = 3;
                break;
            case "C":
                result = 2;
                break;
            case "D":
                result = 1;
                break;
            case "F":
                result = 0;
                break;
            default:
                System.out.println("Wrong grade type.");
                break;
        }
        return result;
    }

    private List<View> createViews() {
        ArrayList<View> result = new ArrayList<View>();

        for (int i = 0; i < classList.size(); i++) {
            Class temp = classList.get(i);

            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = vi.inflate(R.layout.layout_input_assignments, null);

            TextView className = view.findViewById(R.id.class_name_gb);
            className.setText(temp.getName());

            result.add(view);
        }
        return result;
    }
}