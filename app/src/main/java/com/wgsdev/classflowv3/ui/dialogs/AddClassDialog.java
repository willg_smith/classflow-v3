package com.wgsdev.classflowv3.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.database.Class;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddClassDialog extends AppCompatDialogFragment {

    EditText name;
    EditText startDate;
    EditText endDate;
    private int semesterID;
    AppDatabase db = null;

    public AddClassDialog(int semesterID) {
        super();
        this.semesterID = semesterID;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflator = getActivity().getLayoutInflater();
        View view = inflator.inflate(R.layout.layout_add_class_dialog, null);

        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        name = (EditText) view.findViewById(R.id.class_name_d);


        final AppDatabase finalDb = db;
        builder.setView(view).setTitle("Add Class").setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String cName = String.valueOf(name.getText());
                Class temp = new Class(semesterID, cName);
                finalDb.classDAO().insert(temp);


            }
        });


        return builder.create();
    }

    private DialogInterface.OnDismissListener onDismissListener;

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }
}
