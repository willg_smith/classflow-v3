package com.wgsdev.classflowv3.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.Assignment;
import com.wgsdev.classflowv3.database.Class;
import com.wgsdev.classflowv3.database.DatabaseHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddAssignmentDialog extends AppCompatDialogFragment {

    EditText assignmentName;
    EditText assignmentNotes;
    EditText assignmentPointsPossible;
    EditText assignmentPointsEarned;
    EditText assignmentDueDate;
    private int classID;
    AppDatabase db = null;

    public AddAssignmentDialog (int classID) {
        super();
        this.classID = classID;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflator = getActivity().getLayoutInflater();
        View view = inflator.inflate(R.layout.layout_add_assignment_dialog, null);

        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assignmentName = (EditText) view.findViewById(R.id.assignment_name_d);
        assignmentNotes = (EditText) view.findViewById(R.id.assignment_note_d);
        assignmentPointsPossible = (EditText) view.findViewById(R.id.points_possible_d);
        assignmentPointsEarned = (EditText) view.findViewById(R.id.points_earned_d);
        assignmentDueDate = (EditText) view.findViewById(R.id.assignment_due_date_d);


        final AppDatabase finalDb = db;
        builder.setView(view).setTitle("Add Assignment").setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String aName = String.valueOf(assignmentName.getText());
                String note = String.valueOf(assignmentNotes.getText());
                int pointsPossible = Integer.parseInt(String.valueOf(assignmentPointsPossible.getText()));
                int pointsEarned = Integer.parseInt(String.valueOf(assignmentPointsEarned.getText()));
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date due = null;
                try {
                    due = new Date(String.valueOf(formatter.parse(String.valueOf(assignmentDueDate.getText()))));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                boolean isComplete = true;
                if (pointsEarned == 0) isComplete = false;
                long date = due.getTime();
                Assignment temp = new Assignment(classID, aName, note, date, isComplete, pointsPossible, pointsEarned);
                finalDb.assignmentDAO().insert(temp);


            }
        });


        return builder.create();
    }

    private DialogInterface.OnDismissListener onDismissListener;

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }
}
