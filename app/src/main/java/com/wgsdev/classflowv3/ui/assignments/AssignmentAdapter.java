package com.wgsdev.classflowv3.ui.assignments;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.database.Assignment;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.AssignmentViewHolder> {

    List<Assignment> assignmentList;
    private AssignmentAdapter.SelectedAssignment selectedAssignment;

    public AssignmentAdapter(List<Assignment> assignmentList, SelectedAssignment selectedAssignment) {
        this.assignmentList = assignmentList;
        this.selectedAssignment = selectedAssignment;
    }

    public AssignmentAdapter(List<Assignment> assignmentList) {
        this.assignmentList = assignmentList;
    }

    @NonNull
    @Override
    public AssignmentAdapter.AssignmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.assignment_list_layout, parent, false);
        return new AssignmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssignmentAdapter.AssignmentViewHolder holder, int position) {
        Assignment temp = assignmentList.get(position);
        holder.assignmentName.setText(temp.getName());
        String grade = temp.getPointsEarned() + "/" + temp.getPointsPossible();
        holder.assignmentGrade.setText(grade);
        String note = temp.getNote();
        if (!StringUtils.isAllBlank(note)) {
            holder.assignmentNote.setText(note);
        }
        long due = temp.getDueDate();
        Date date = new Date(due);
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        Date current = new Date();
        String finalDate = null;
        int daysUntilDue = (int) ((date.getTime() - current.getTime())/(1000*60*24*60)) + 1;

        if(true) {
            if (daysUntilDue < 0 && !temp.isCompleted()) {
                finalDate = "Due on " + format.format(date) + " - past due by " + (Math.abs(daysUntilDue)) + " days";
            }
            else if (temp.isCompleted()) {
                finalDate = "Assignment complete - due on " + format.format(date);
            }
            else if (daysUntilDue >= 0){
                finalDate = "Due on " + format.format(date) + " - " + daysUntilDue + " days";
            }
        }

        holder.assignmentDueDate.setText(finalDate);

    }

    @Override
    public int getItemCount() {
        return assignmentList.size();
    }

    public void setData(List<Assignment> assignmentList) {
        this.assignmentList = assignmentList;
    }

    public class AssignmentViewHolder extends RecyclerView.ViewHolder {

        public TextView assignmentName;
        public TextView assignmentGrade;
        public TextView assignmentNote;
        public TextView assignmentDueDate;
        public AssignmentViewHolder(@NonNull View itemView) {
            super(itemView);
            assignmentName = itemView.findViewById(R.id.assignment_name);
            assignmentGrade = itemView.findViewById(R.id.assignment_grade);
            assignmentNote = itemView.findViewById(R.id.assignment_note);
            assignmentDueDate = itemView.findViewById(R.id.assignment_due_date);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectedAssignment.selectedAssignment(assignmentList.get(getAdapterPosition()));
                }
            });


        }
    }

    public interface SelectedAssignment {
        void selectedAssignment(Assignment assignment);
    }
}
