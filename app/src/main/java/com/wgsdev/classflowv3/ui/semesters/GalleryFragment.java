package com.wgsdev.classflowv3.ui.semesters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wgsdev.classflowv3.MainActivity;
import com.wgsdev.classflowv3.R;
import com.wgsdev.classflowv3.ui.classes.ClassView;
import com.wgsdev.classflowv3.database.AppDatabase;
import com.wgsdev.classflowv3.database.DatabaseHolder;
import com.wgsdev.classflowv3.database.Semester;
import com.wgsdev.classflowv3.ui.dashboard.HomeFragment;
import com.wgsdev.classflowv3.ui.dialogs.AddSemesterDialog;

import java.util.List;

public class GalleryFragment extends Fragment implements SemesterAdapter.SelectedSemester {

    RecyclerView recyclerView;
    SemesterAdapter adapter;
    List<Semester> semesterList;
    AppDatabase db = null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.semesters_home, container, false);

        Button addSemester = (Button) view.findViewById(R.id.add_semester);

        addSemester.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(getContext(), AddSemester.class);
                //startActivity(intent);
                openDialog();

            }
        });

        try {
            db = DatabaseHolder.getRoomDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        semesterList = db.semesterDAO().getAllSemesters();

        recyclerView = (RecyclerView) view.findViewById(R.id.semesterRecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        adapter = new SemesterAdapter(semesterList, this);
        recyclerView.setAdapter(adapter);

        return view;
    }

    private void openDialog() {
        AddSemesterDialog add = new AddSemesterDialog();
        add.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                adapter.setData(db.semesterDAO().getAllSemesters());
                adapter.notifyDataSetChanged();
                recyclerView.setAdapter(adapter);
            }
        });
        add.show(getActivity().getSupportFragmentManager(), "Add Semester");
    }

    @Override
    public void selectedSemester(Semester semester) {

        startActivity(new Intent(getActivity(), ClassView.class).putExtra("data", semester));
    }

}