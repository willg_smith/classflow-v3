package com.wgsdev.classflowv3.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Semester.class, parentColumns = "semester_id", childColumns = "semester_id", onDelete = CASCADE))
public class Class implements Serializable {

    // Fields self-explanatory

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "class_id")
    private int classID;

    @ColumnInfo(name = "semester_id")
    private int semesterID;

    @ColumnInfo(name = "name")
    private String name;

    public Class(int semesterID, String name) {
        this.semesterID = semesterID;
        this.name = name;
    }

    public Class(Semester semester, String name) {
        this.semesterID = semester.getId();
        this.name = name;
    }


    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSemesterID() {
        return semesterID;
    }

    public void setSemesterID(int semesterID) {
        this.semesterID = semesterID;
    }

}
