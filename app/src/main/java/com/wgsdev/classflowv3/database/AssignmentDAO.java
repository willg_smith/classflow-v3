package com.wgsdev.classflowv3.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AssignmentDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Assignment assignmentToInsert);

    @Update
    void update(Assignment assignmentToUpdate);

    @Delete
    void remove(Assignment assignmentToRemove);

    @Query("SELECT * FROM assignment")
    List<Assignment> getAllAssignments();

    @Query("SELECT * FROM assignment WHERE due_date > :currentDate ORDER BY due_date")
    List<Assignment> getAllUpcomingAssignments(long currentDate);

    @Query("SELECT * FROM assignment WHERE due_date < :currentDate ORDER BY due_date DESC")
    List<Assignment> getAllPastAssignments(long currentDate);

    @Query("SELECT * FROM assignment WHERE assignment_id = :assignmentID")
    Assignment getAssignment(int assignmentID);

    @Query("SELECT * FROM assignment WHERE class_ID = :classID ORDER BY due_date DESC")
    List<Assignment> getAssignmentsByClass(int classID);

    @Query("SELECT * FROM assignment WHERE class_ID = :classID ORDER BY due_date ASC")
    List<Assignment> getAssignmentsByClassAsc(int classID);

    @Query("INSERT INTO assignment (class_ID, name, note, due_date, completed) " +
            "VALUES (:classID, :name, :note, :dueDate, :isComplete)")
    void insert(int classID, String name, String note, long dueDate, boolean isComplete);

    @Query("UPDATE assignment SET name = :newName WHERE assignment_id = :assignmentID")
    void changeAssignmentName(int assignmentID, String newName);

    @Query("UPDATE assignment SET note = :newNote WHERE assignment_id = :assignmentID")
    void changeAssignmentNote(int assignmentID, String newNote);

    @Query("UPDATE assignment SET due_date = :newDate WHERE assignment_id = :assignmentID")
    void changeAssignmentDueDate(int assignmentID, long newDate);

    @Query("UPDATE assignment SET completed = :isComplete WHERE assignment_id = :assignmentID")
    void changeAssignmentCompleted(int assignmentID, boolean isComplete);
}
