package com.wgsdev.classflowv3.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ClassDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Class classToInsert);

    @Update
    void update(Class classToUpdate);

    @Delete
    void remove(Class classToRemove);

    @Query("SELECT * FROM class")
    List<Class> getAllClasses();

    @Query("SELECT * FROM class where class.semester_id in" +
            "(SELECT semester_id FROM semester where semester_id = :semesterID)")
    List<Class> getClassesBySemesterID(int semesterID);

    @Query("SELECT * FROM class WHERE class_id = :classID")
    Class getClass(int classID);

    @Query("UPDATE class SET name = :name WHERE class_id = :classID")
    void updateName(int classID, String name);

    @Query("DELETE FROM class WHERE class_id = :classID")
    void deleteClass(int classID);
}
