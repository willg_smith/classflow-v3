package com.wgsdev.classflowv3.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Semester implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "semester_id")
    private int id;

    @ColumnInfo(name = "name")
    private String name;    // example "Spring 2020" User will specify in "Semester name" field.

    @ColumnInfo(name = "start_date")
    private long startDate; // ALL DATES ARE STORE IN MILLISECONDS

    @ColumnInfo(name = "end_date")
    private long endDate;   // ALL DATES ARE STORE IN MILLISECONDS

    @Ignore
    private double gpa;

    public Semester(String name, long startDate, long endDate) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Ignore
    public Semester(String name) {
        this.name = name;
        this.gpa = 4.0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    @Ignore
    public double getGpa() {
        return gpa;
    }
}
