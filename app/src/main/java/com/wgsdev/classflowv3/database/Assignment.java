package com.wgsdev.classflowv3.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Class.class, parentColumns = "class_id", childColumns = "class_id", onDelete = CASCADE))
public class Assignment implements Serializable {


    // Fields self-explanatory
    @ColumnInfo(name = "assignment_id")
    @PrimaryKey(autoGenerate = true)
    private int assignmentID;

    @ColumnInfo(name = "class_id")
    private int classID;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "note")
    private String note;

    @ColumnInfo(name = "due_date") // ALL DATES ARE STORE IN MILLISECONDS
    private long dueDate;

    @ColumnInfo(name = "completed")
    private boolean completed;

    @ColumnInfo(name = "points_possible")
    private int pointsPossible;

    @ColumnInfo(name = "points_earned")
    private int pointsEarned;

    public Assignment(int classID, String name, String note, long dueDate, boolean completed, int pointsPossible, int pointsEarned) {
        this.classID = classID;
        this.name = name;
        this.note = note;
        this.dueDate = dueDate;
        this.completed = completed;
        this.pointsPossible = pointsPossible;
        this.pointsEarned = pointsEarned;
    }

    public Assignment(Class parentClass, String name, String note, long dueDate, boolean completed, int pointsPossible, int pointsEarned) {
        this.classID = parentClass.getClassID();
        this.name = name;
        this.note = note;
        this.dueDate = dueDate;
        this.completed = completed;
        this.pointsEarned = pointsEarned;
        this.pointsPossible = pointsPossible;
    }

    public int getId() {
        return assignmentID;
    }

    public void setId(int assignmentID) {
        this.assignmentID = assignmentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }

    public int getAssignmentID() {
        return assignmentID;
    }

    public void setAssignmentID(int assignmentID) {
        this.assignmentID = assignmentID;
    }

    public int getPointsPossible() {
        return pointsPossible;
    }

    public void setPointsPossible(int pointsPossible) {
        this.pointsPossible = pointsPossible;
    }

    public int getPointsEarned() {
        return pointsEarned;
    }

    public void setPointsEarned(int pointsEarned) {
        this.pointsEarned = pointsEarned;
    }
}
