package com.wgsdev.classflowv3.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {User.class, Semester.class, Class.class, Assignment.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDAO userDAO();
    public abstract SemesterDAO semesterDAO();
    public abstract ClassDAO classDAO();
    public abstract AssignmentDAO assignmentDAO();
}
