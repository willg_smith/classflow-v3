package com.wgsdev.classflowv3.database;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDAO {

    @Query("SELECT * FROM user")
    List<User> getAllUsers();

    @Query("SELECT * FROM user WHERE id = 0")
    User getPrimaryUser();

    @Query("UPDATE user SET first_name = :firstName, last_name = :lastName," +
            "school = :school, bio = :bio, graduation_date = :graduationDate " +
            "WHERE id = 0")
    void updateUser(String firstName, String lastName, String school,
                              String bio, long graduationDate);

    @Query("INSERT INTO user (first_name, last_name, school, bio, graduation_date) " +
            "VALUES (:firstName, :lastName, :school, :bio, :graduationDate)")
    void addUser(String firstName, String lastName, String school, String bio, long graduationDate);

    @Query("UPDATE user SET first_name = :firstName WHERE id = 0")
    void updateFirstName(String firstName);

    @Query("UPDATE user SET last_name = :lastName WHERE id = 0")
    void updateLastName(String lastName);

    @Query("UPDATE user SET first_name = :firstName, last_name = :lastName WHERE id = 0")
    void updateFullName(String firstName, String lastName);

    @Query("UPDATE user SET school = :school WHERE id = 0")
    void updateSchool(String school);

    @Query("UPDATE user SET bio = :bio WHERE id = 0")
    void updateBio(String bio);

    @Query("UPDATE user SET graduation_date = :graduationDate WHERE id = 0")
    void updateGraduationDate(long graduationDate);
}
