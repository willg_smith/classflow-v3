package com.wgsdev.classflowv3.database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

public class DatabaseHolder {

    private static AppDatabase db;

    private DatabaseHolder() {

    }

    public static void initializeAppDatabase(Context context) {
        if (db == null) {
            db = Room.databaseBuilder(context, AppDatabase.class, "beta")
                    .allowMainThreadQueries() // This needs to be on background thread for production
                    .build();
        }
    }

    public static AppDatabase getRoomDatabase() throws Exception {
        if (db == null) {
            throw new Exception("Database not initialized. Please initialize database first.");
        }
        return db;
    }
}
