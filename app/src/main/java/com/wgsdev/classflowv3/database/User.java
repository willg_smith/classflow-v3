package com.wgsdev.classflowv3.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {

    // Fields self-explanatory

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "first_name")
    private String firstName;

    @ColumnInfo(name = "last_name")
    private String lastName;

    @ColumnInfo(name = "school")
    private String school;

    @ColumnInfo(name = "bio")
    private String bio;

    @ColumnInfo(name = "graduation_date")
    private long graduationDate;

    public User(String firstName, String lastName, String school, String bio, long graduationDate) {
        this.firstName = firstName;
        this.bio = bio;
        this.graduationDate = graduationDate;
        this.lastName = lastName;
        this.school = school;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public long getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(long graduationDate) {
        this.graduationDate = graduationDate;
    }





}
