package com.wgsdev.classflowv3.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;


import java.util.List;

@Dao
public interface SemesterDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Semester semesterToInsert);

    @Update
    void update(Semester semesterToUpdate);

    @Delete
    void remove(Semester semesterToRemove);

    @Query("SELECT * FROM semester ORDER BY start_date DESC")
    List<Semester> getAllSemesters();

    @Query("SELECT * FROM semester where semester_id = :semesterID")
    Semester getSemester(int semesterID);

    @Query("DELETE FROM semester WHERE semester_id=:semesterID")
    void removeSemester(int semesterID);

    @Query("UPDATE semester SET name = :name WHERE semester_id = :semesterID")
    void changeName(int semesterID, String name);

    @Query("UPDATE semester SET start_date = :newDate WHERE semester_id = :semesterID")
    void changeStartDate(int semesterID, long newDate);

    @Query("UPDATE semester SET end_date = :newDate WHERE semester_id = :semesterID")
    void changeEndDate(int semesterID, long newDate);
}
